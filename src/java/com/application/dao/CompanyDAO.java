/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.dao;

import com.application.model.Company;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Asdrubal
 */
public class CompanyDAO extends ConnectionDAO {
    
    private static final String ADDCOMPANY_SQL ="INSERT INTO COMPANY(COMPANY_NAME, COMPANY_RIF, COMPANY_PHONE, COMPANY_RESPONSIBLE, COMPANY_ADDRESS) VALUES (?,?,?,?,?) ";

    public void addCompany(String nameCompany,String rifCompany,String phone,String responsable,String direction) throws Exception {
        PreparedStatement ps = null;
        try {
            getConnection(false);
            ps = connection.prepareStatement(ADDCOMPANY_SQL);
            
            //for(int i = 0, n = data.length; i < n; i++){
                ps.setString(1, nameCompany);
                ps.setString(2, rifCompany);
                ps.setString(3, phone);
                ps.setString(4, responsable);
                ps.setString(5, direction);
            //}
            ps.execute();
            System.out.println(ps.toString());
        } catch (SQLTimeoutException sqltoe) {
            throw new Exception(timeoutError);
        } catch (SQLException sqle) {
            throw new Exception(sqlError);
        } finally {
            closeStatement(ps);
            closeConnection();
        }
    }
}
