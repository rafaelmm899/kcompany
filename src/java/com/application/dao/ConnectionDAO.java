/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author Asdrubal
 */
public abstract class ConnectionDAO {
    
    protected Connection connection;
    private static String    LOCAL_SERVER_NAME = "10.0.1.109";
    private static int       LOCAL_PORT = 3306;
    private static String    LOCAL_DATA_BASE = "kino";
    private static String    LOCAL_USER_NAME = "mycrocomsys";
    private static String    LOCAL_PASSWORD = "mycrocomsys";
    private static String    LOCAL_DRIVER_NAME = "com.mysql.jdbc.Driver";

    protected static final String timeoutError = "Tiempo de espera agotado para acceder al servidor de datos.";
    protected static final String sqlError = "Ha ocurrido un error de SQL al intentar acceder al servidor de datos.";

    private static final String driverNotFoundError = "El driver especificado para la conexión no se ha encontrado.";
    private static final String failedOpenConnectionError = "No se ha podido establecer conexión con el servidor de datos.\nPor favor, configuere los parámetros de conexión del servidor.";
    private static final String failedCloseConnectionError = "Imposible cerrar la conexión con el servidor de datos.";
    private static final String failedCloseStatementError = "La sentencia SQL para esta acción no se pudo cerrar.";
    private static final String failedCloseResultSetError = "El resultado obtenido de la consulta SQL no se pudo cerrar.";
    private static final String failedRollbackError = "La transacción no se pudo revertir.";
         
         
         

    public ConnectionDAO() {
        
    }
    
    public static void parameters(String SERVER_NAME, int PORT, String DATA_BASE, String USER_NAME, String PASSWORD, String DRIVER_NAME) {
        ConnectionDAO.LOCAL_SERVER_NAME = SERVER_NAME;
        ConnectionDAO.LOCAL_PORT = PORT;
        ConnectionDAO.LOCAL_DATA_BASE = DATA_BASE;
        ConnectionDAO.LOCAL_USER_NAME = USER_NAME;
        ConnectionDAO.LOCAL_PASSWORD = PASSWORD;
        ConnectionDAO.LOCAL_DRIVER_NAME = DRIVER_NAME;
    }

    protected void getConnection(boolean transactional) throws Exception {
        try {                    
            Class.forName(LOCAL_DRIVER_NAME);
            connection = DriverManager.getConnection("jdbc:mysql://"+ LOCAL_SERVER_NAME + ":" + LOCAL_PORT + "/" + LOCAL_DATA_BASE, LOCAL_USER_NAME, LOCAL_PASSWORD);
            connection.setAutoCommit(!transactional);
        } catch (ClassNotFoundException ex) {
            throw new Exception(driverNotFoundError);
        } catch (SQLException ex) {
            throw new Exception(failedOpenConnectionError);
        }        
    }
    protected void closeConnection() throws Exception {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                throw new Exception(failedCloseConnectionError);
            }
        }
    }
    
    protected void closeStatement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException e) {
                System.err.println(failedCloseStatementError);
            }
        }
    }
    
    protected void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                System.err.println(failedCloseResultSetError);
            }
        }
    }
    
    protected void revertTransaction() {
        try {                
            connection.rollback();
        } catch (SQLException ex) {
            System.err.println(failedRollbackError);
        }
    }  
    
}
