/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.dao;

import com.application.model.Agency;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Francys ID_AGENCY, ID_COMPANY, AGENCY_NAME, AGENCY_RIF, AGENCY_PHONE, AGENCY_RESPONSIBLE
 */
public class AgencyDAO extends ConnectionDAO {
    
    private static final String FIND_ALL_SQL="SELECT ID_AGENCY, ID_COMPANY, AGENCY_NAME, AGENCY_RIF, AGENCY_PHONE, AGENCY_RESPONSIBLE  FROM AGENCY ORDER BY AGENCY_NAME ASC;";
    
    public List<Agency> findAll() throws Exception {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Agency> resultList = new ArrayList<>();
        try {                        
            getConnection(false);            
            ps = connection.prepareStatement(FIND_ALL_SQL);
            rs = ps.executeQuery();
            while(rs.next()){
                resultList.add(new Agency(rs.getInt("ID_AGENCY"), rs.getInt("ID_COMPANY"), rs.getString("AGENCY_NAME"),rs.getString("AGENCY_RIF"),rs.getString("AGENCY_PHONE"),rs.getString("AGENCY_RESPONSIBLE")));
            }

        } catch (SQLTimeoutException sqltoe) {
            throw new Exception(timeoutError);
        } catch (SQLException sqle) {
            throw new Exception(sqlError);
        } finally {
            closeResultSet(rs);
            closeStatement(ps);
            closeConnection();
        }
        return resultList; 
    }
  
}
