/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.servlet;

import com.application.service.AgencyService;
import com.application.model.Agency;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Francys
 * 
 * 
 */
public class AgencyServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        request.getRequestDispatcher("/WEB-INF/jsp/addAgency.jsp").forward(request,response);
    }
    
    private void findAll(HttpServletRequest request, HttpServletResponse response)  throws Exception{
        AgencyService agencyService = new AgencyService();
        List<Agency> agencyList = agencyService.findAll();
        request.setAttribute("list", agencyList);
        getServletContext().getRequestDispatcher("/WEB-INF/jsp/addAgency.jsp").forward(request, response);
        
    }

}
