package com.application.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Asdrubal
 */
public class Company {
     private int idCompany;
     private String companyName;
     private String companyRif;
     private String companyPhone;
     private String companyResponsible;
     private String companyAddress;

    public Company() {
    }

    public Company(String companyName, String rif, int idCompany) {
        this.companyName = companyName;
        this.companyRif = companyRif;
        this.idCompany = idCompany;
        this.companyPhone = companyPhone;
        this.companyResponsible = companyResponsible;
        this.companyAddress = companyAddress;
        
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }
     
    public String getCompanyRif() {
        return companyRif;
    }
     
    public void setCompanyRif(String companyRif) {
        this.companyRif = companyRif;
}

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCompanyResponsible() {
        return companyResponsible;
    }

    public void setCompanyResponsible(String companyResponsible) {
        this.companyResponsible = companyResponsible;
    }
    
    
    
    
    
    


    
    
    
     
     
}
