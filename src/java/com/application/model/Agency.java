/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.model;

/**
 *
 * @author Francys
 * ID_AGENCY, ID_COMPANY, AGENCY_NAME, AGENCY_RIF, AGENCY_PHONE, AGENCY_RESPONSIBLE
 */
public class Agency { 
    
    private int idAgency;
    private int idCompany;
    private String agencyName;
    private String agencyRif;
    private String agencyPhone;
    private String agencyResponsible;  

    public Agency() {
    }

    public Agency(int idAgency, int idCompany, String agencyName, String agencyRif, String agencyPhone, String agencyResponsible) {
        this.idAgency = idAgency;
        this.idCompany = idCompany;
        this.agencyName = agencyName;
        this.agencyRif = agencyRif;
        this.agencyPhone = agencyPhone;
        this.agencyResponsible = agencyResponsible;
    }

    public int getIdAgency() {
        return idAgency;
    }

    
    public void setIdAgency(int idAgency) {
        this.idAgency = idAgency;
    }

    public int getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(int idCompany) {
        this.idCompany = idCompany;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getAgencyRif() {
        return agencyRif;
    }

    public void setAgencyRif(String agencyRif) {
        this.agencyRif = agencyRif;
    }

    public String getAgencyPhone() {
        return agencyPhone;
    }

    public void setAgencyPhone(String agencyPhone) {
        this.agencyPhone = agencyPhone;
    }

    public String getAgencyResponsible() {
        return agencyResponsible;
    }

    public void setAgencyResponsible(String agencyResponsible) {
        this.agencyResponsible = agencyResponsible;
    }
    
    
}
