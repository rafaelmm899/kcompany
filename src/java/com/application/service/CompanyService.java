/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.service;

import com.application.dao.CompanyDAO;
import com.application.model.Company;
/**
 *
 * @author Asdrubal
 */
public class CompanyService {
    
    private CompanyDAO companyDAO;
    
    public CompanyService(){
        this.companyDAO = new CompanyDAO();
    }


    public void  addCompany(String nameCompany,String rifCompany,String phone,String responsable,String direction) throws Exception {
        companyDAO.addCompany(nameCompany,rifCompany,phone,responsable,direction);
    }
    
}
