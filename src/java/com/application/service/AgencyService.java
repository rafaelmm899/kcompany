/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.service;

import com.application.dao.AgencyDAO;
import com.application.model.Agency;
import java.util.List;

/**
 *
 * @author MycrocomSistema04
 */
public class AgencyService {
    
    private AgencyDAO agencyDAO;
    
    public AgencyService () {
        this.agencyDAO = new AgencyDAO();
    }
    
    public List<Agency> findAll() throws Exception {
        return agencyDAO.findAll();
    }  
}
