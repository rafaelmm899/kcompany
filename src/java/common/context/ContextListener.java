package common.context;

import com.application.dao.ConnectionDAO;
import java.util.Locale;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener()
public class ContextListener implements ServletContextListener, HttpSessionListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        Locale.setDefault(new Locale("es", "VE"));
        
        ServletContext sc = sce.getServletContext();
        String SERVER_NAME = sc.getInitParameter("SERVER_NAME");
        int PORT = Integer.parseInt(sc.getInitParameter("PORT"));
        String DATA_BASE = sc.getInitParameter("DATA_BASE");
        String USER_NAME = sc.getInitParameter("USER_NAME");
        String PASSWORD = sc.getInitParameter("PASSWORD");
        String DRIVER_NAME = sc.getInitParameter("DRIVER_NAME");
        ConnectionDAO.parameters(SERVER_NAME, PORT, DATA_BASE, USER_NAME, PASSWORD, DRIVER_NAME);
       
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        
    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
