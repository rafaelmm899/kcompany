package common.utils;
/**
 *
 * @author 
 */
public class TracerException extends Exception {
        
    public TracerException() {
        super();
    }

    public TracerException(String message) {
        super(message);
    }

    public TracerException(Throwable cause) { 
        super(cause);
    }
    
    public TracerException(String message, Throwable cause) {
        super(message, cause);
    }

    public TracerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);        
    }
    
    public String getFullTracer() {
        return getFullTracer(this);
    }
    
    public static String getFullTracer(Exception ex) {
        StringBuilder sb = new StringBuilder();
        sb.append(ex.getMessage());
        for (StackTraceElement element : ex.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");            
        }
        return sb.toString();
    }
    
    public String getSimpleTracer() {
        StringBuilder sb = new StringBuilder();        
        String className = getCurrentClass(3);                
        sb.append(getTracer(this.getStackTrace(), className));
        sb.append(this.getMessage());
        return sb.toString();
    }
    
    public static String getSimpleTracer(Exception ex) {
        StringBuilder sb = new StringBuilder();        
        String className = getCurrentClass(3);                
        sb.append(getTracer(ex.getStackTrace(), className));
        sb.append(ex.getMessage());
        return sb.toString();
    }
    
    public String getSimpleTracer(String className) {
        return getSimpleTracer(this, className);
    }
    
    public static String getSimpleTracer(Exception ex, String className) {
        StringBuilder sb = new StringBuilder();               
        sb.append(getTracer(ex.getStackTrace(), className)); 
        sb.append(ex.getMessage());
        return sb.toString();
    }
    
    private static String getTracer(StackTraceElement[] elements, String className) {        
        for (StackTraceElement element : elements)             
            if (element.getClassName().contains(className)) 
                return element.toString() + " ";                            
        return "";      
    }
    
    private static String getCurrentClass(int level) {
        StackTraceElement []elements = Thread.currentThread().getStackTrace();               
        if (elements.length > level)
            return elements[level].getClassName();
        return "";
    }
    
}
