<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title></title>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/simple-sidebar.css" rel="stylesheet">
        <link href="css/font-awesome.css" rel="stylesheet">
        
    </head>
    <body>
        <div id="wrapper">
            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar-nav">
                    <li class="sidebar-brand">
                     <a><i class="fa fa-television" aria-hidden="true"></i>&nbsp;Admin</a>
                     <li>
                        <a data-toggle="collapse" data-target="#admin"><i class="fa fa-cog" aria-hidden="true"></i>&nbsp;Administraci�n</a>
                        <div id="admin" class="sublinks collapse">
                            <a data-toggle="collapse" data-target="#roles"> <i class="fa fa-tasks" ></i>&nbsp;Roles&nbsp;</a>
                        </div>
                         <div id="roles" class="sublinks collapse">
                            <a><i class="fa fa-plus-square-o"></i>&nbsp;Agregar rol&nbsp;</a>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" data-target="#user" ><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Usuarios</a>
                        <div id="user" class="sublinks collapse">
                            <a class=""><i class="fa fa-user-plus"></i>&nbsp;Agregar&nbsp;</a>
                            <a class=""><span class="fa fa-pencil-square-o"></span>&nbsp;Modificar</a>
                            <a class=""><span class="fa  fa-user-times"></span>&nbsp;Eliminar</a>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" data-target="#company" ><i class="fa fa-building" aria-hidden="true"></i>&nbsp;Compa�ias</a>
                        <div id="company" class="sublinks collapse">
                            <a href="addCompany.jsp" class=""><span class="glyphicon glyphicon-chevron-right"></span> inbox</a>
                            <a class=""><span class="glyphicon glyphicon-chevron-right"></span> sent</a>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" data-target="#agency" ><i class="fa fa-group" aria-hidden="true"></i>&nbsp;Agencias</a>
                        <div id="agency" class="sublinks collapse">
                            <a class=""><span class="glyphicon glyphicon-chevron-right"></span> inbox</a>
                            <a class=""><span class="glyphicon glyphicon-chevron-right"></span> sent</a>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" data-target="#pos" ><i class="fa fa-laptop" aria-hidden="true"></i>&nbsp;Puntos de venta</a>
                        <div id="pos" class="sublinks collapse">
                            <a class=""><span class="glyphicon glyphicon-chevron-right"></span> inbox</a>
                            <a class=""><span class="glyphicon glyphicon-chevron-right"></span> sent</a>
                        </div>
                    </li>
                    <li>
                        <a data-toggle="collapse" data-target="#report" ><i class="fa fa-folder-open" aria-hidden="true"></i>&nbsp;Reportes de venta</a>
                        <div id="report" class="sublinks collapse">
                            <a class=""><span class="glyphicon glyphicon-chevron-right"></span> inbox</a>
                            <a class=""><span class="glyphicon glyphicon-chevron-right"></span> sent</a>
                        </div>
                    </li> 
                    <br><br><br>
                    <li>
                        <a><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; Cerrar sesi�n</a>
                    </li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading col-lg-12"><span style="cursor: pointer" href="#menu-toggle" id="menu-toggle" class="fa fa-bars col-lg-8" aria-hidden="true"></span><span class="col-lg-4" > NOMBRE DE USUARIO</span></div> 
                <br>
                <br>
                <br>
                <div class="panel-body">
                    <div class="panel-heading"><h3><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp; Registro de compa�ias</h3></div>
                    <div class="panel panel-default">
                     
                        <form class="form-horizontal" id="frmCompany" name="frmCompany">
                            <br>
                            <div class="form-group">
                                <label class="control-label col-xs-3">Rif:</label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="rifCompany" name="rifCompany" placeholder="J-0000000-0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3">Compa�ia</label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="nameCompany" name="nameCompany" placeholder="Nombre de la compa�ia">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3">Responsable</label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="responsable" name="responsable" placeholder="Responsable">
                                </div>
                            </div>
                           <div class="form-group">
                                <label class="control-label col-xs-3" >Estado:</label>
                                <div class="col-xs-9">
                                    <select class="form-control"id="estate" name="estate">
                                        <option>user</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" >Municipio:</label>
                                <div class="col-xs-9">
                                    <select class="form-control"id="municipio" name="municipio">
                                        <option>user</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" >Ubicaci�n:</label>
                                <div class="col-xs-9">
                                    <select class="form-control" id="direction" name="direction">
                                        <option>user</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" >Tel�fono:</label>
                                <div class="col-xs-9">
                                    <input type="number" class="form-control" id="phone" name="phone" placeholder="Tel�fono">
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="col-xs-offset-3 col-xs-9">
                                    <input type="button" class="btn btn-primary" id="addCompany" name="addCompany" value="Agregar">
                                    <input type="reset" class="btn btn-default" value="Limpiar">
                                </div>
                            </div>
                        </form>
                    </div>                                              
                </div>    
            </div> <!-- cierre panel default -->
        </div> <!-- cierre graper -->
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.bootpag.min.js"></script>
        <script src="js/company.js"></script>

    
        <!-- Menu Toggle Script -->
    </body>
</html>
