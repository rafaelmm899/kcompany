$(document).ready(function(){
    $("#menu-toggle").click(function (e) 
    {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    
    function sendAjax(urlSend, typeRequest, dataToSend, doneResponse, bSend, failResponse){
        $.ajax({
            url: urlSend,
            type: typeRequest,
            data: {"data" : dataToSend},
            done: function(output) 
            {
                console.log(output);
                doneResponse;
            },
            fail:function() 
            {
                console.log("error");
                failResponse;
            },
            beforeSend:function() 
            {
                console.log("complete");
                bSend;
            }
        });
    }
    
    $('#addUser').on('click', function(){
        var array = Array($('#nameUser').val(), $('#lastName').val(), $('#user').val(),$('#password').val(),$('#typeUser').val());
        sendAjax('./CompanyServlet', 'POST', array);
    });
    
});
