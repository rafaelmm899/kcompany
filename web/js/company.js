$(document).ready(function(){
    $("#menu-toggle").click(function (e) 
    {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    
    function sendAjax(urlSend, typeRequest, dataToSend, doneResponse, bSend, failResponse){
        $.ajax({
            url: urlSend,
            type: typeRequest,
            data: dataToSend,
            done: function(output) 
            {
                console.log(output);
                doneResponse;
            },
            fail:function() 
            {
                console.log("error");
                failResponse;
            },
            beforeSend:function() 
            {
                console.log("complete");
                bSend;
            }
        });
    }
    
    $('#addCompany').on('click', function(){
        var array = [];
        array.push($('#nameCompany').val());
        array.push($('#rifCompany').val());
        array.push($('#phone').val());
        array.push($('#responsable').val());
        array.push($('#direction').val());
        
        //var array = new Array($('#nameCompany').val(), $('#rifCompany').val(), $('#phone').val(), $('#responsable').val(), $('#direction').val());
        sendAjax('./CompanyServlet', 'POST', {nameCompany:$('#nameCompany').val(), rifCompany:$('#rifCompany').val(),phone:$('#phone').val(),responsable:$('#responsable').val(),direction:$('#direction').val()});
    });
    
});